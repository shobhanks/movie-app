var CineTicketApp = angular.module('CineTicketApp', []);

CineTicketApp.controller('mapTheaterController', function($scope, $http) {
	$scope.theaters = [];

	$scope.search = function() {
		//remove current results
		$scope.theaters = [];
		$scope.messageText="";
		
		//search
		$http.get('/CineTicket3/rest/searchTheater',		
			{params: {
			zipCode:$scope.searchByZipCode}})
			.success(function(data, status) {
				$scope.httpStatus = status;
				$scope.httpData = data;
				$scope.errorStatus=false;
				$scope.messageText="Found "+data.length;
				$scope.theaters = data;
			})
			.error(function(data, status) {
				$scope.httpStatus = status;				
				$scope.httpData = data;
				$scope.errorStatus=true;
				$scope.messageText=data.error.code+ " "+ data.error.message;
			});	
		$scope.searchByZipCode='';
	};
});
