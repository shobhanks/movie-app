var CineTicketApp = angular.module('CineTicketApp', []);

CineTicketApp.controller('addMovieController', function($scope, $http) {

	$scope.add = function() {
		$scope.messageText="";
		
		$http.post('/CineTicket3/rest/admin/addmovie',[{"imdbids":$scope.imdbIds}])
			.success(function(data, status) {
				$scope.httpStatus = status;
				$scope.httpData = data;
				$scope.errorStatus=false;
				$scope.messageText=data;
			})
			.error(function(data, status) {
				$scope.httpStatus = status;				
				$scope.httpData = data;
				$scope.errorStatus=true;
				$scope.messageText=data.error.code+ " "+ data.error.message;
			});		
	};
});
