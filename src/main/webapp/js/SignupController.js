var CineTicketApp = angular.module('CineTicketApp', []);

CineTicketApp.controller('signupController', function($scope, $http) {

	$scope.signup = function() {
		$scope.messageText="";
		
		$http.post('/CineTicket3/rest/account/signup',[{
			  "uname": $scope.uname,
		        "pwd": $scope.pwd,
		        "email_id": $scope.email_id,
		        "zipcode": $scope.zipcode,
		        "fav_genre":$scope.fav_genre}])
			.success(function(data, status) {
				$scope.httpStatus = status;
				$scope.httpData = data;
				$scope.errorStatus=false;
				$scope.messageText=data;
			})
			.error(function(data, status) {
				$scope.httpStatus = status;				
				$scope.httpData = data;
				$scope.errorStatus=true;
				$scope.messageText=data.error.code+ " "+ data.error.message;
			});		
	};
});
