var CineTicketApp = angular.module('CineTicketApp', []);

CineTicketApp.controller('signinController', function($scope, $http) {

	$scope.signin = function() {
		$scope.messageText="";
		
		$http.get('/CineTicket3/rest/account/login',		
			{params: {
				username:$scope.userName,
				password:$scope.password}})
			.success(function(data, status) {
				$scope.httpStatus = status;
				$scope.httpData = data;
				$scope.errorStatus=false;
				$scope.messageText=data;
			})
			.error(function(data, status) {
				$scope.httpStatus = status;				
				$scope.httpData = data;
				$scope.errorStatus=true;
				$scope.messageText=data.error.code+ " "+ data.error.message;
			});		
	};
});
