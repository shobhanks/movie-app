var CineTicketApp = angular.module('CineTicketApp', []);

CineTicketApp.controller('searchTrailerController', function($scope, $http) {
	$scope.trailers = [];

	$scope.search = function() {
		//remove current results
		$scope.trailers = [];
		$scope.messageText="";
		
		//search
		$http.get('/CineTicket3/rest/trailer',		
			{params: {
				title:$scope.searchByTitle}})
			.success(function(data, status) {
				$scope.httpStatus = status;
				$scope.httpData = data;
				$scope.errorStatus=false;
				$scope.messageText="Found "+data.length;
				$scope.trailers = data;
			})
			.error(function(data, status) {
				$scope.httpStatus = status;				
				$scope.httpData = data;
				$scope.errorStatus=true;
				$scope.messageText=data.error.code+ " "+ data.error.message;
			});	
		$scope.searchByTitle='';
	};
});
