var CineTicketApp = angular.module('CineTicketApp', []);

CineTicketApp.controller('searchController', function($scope, $http) {
	$scope.movies = [];

	$scope.search = function() {
		//remove current results
		$scope.movies = [];
		$scope.messageText="";
		
		//search
		$http.get('/CineTicket3/rest/search',		
			{params: {
				title:$scope.searchByTitle,
				actor:$scope.searchByActor,
				genre:$scope.searchByGenre}})
			.success(function(data, status) {
				$scope.httpStatus = status;
				$scope.httpData = data;
				$scope.errorStatus=false;
				$scope.messageText="Found "+data.length;
				$scope.movies = data;
			})
			.error(function(data, status) {
				$scope.httpStatus = status;				
				$scope.httpData = data;
				$scope.errorStatus=true;
				$scope.messageText=data.error.code+ " "+ data.error.message;
			});	
		$scope.searchByTitle='';
		$scope.searchByActor='';
		$scope.searchByGenre='';
	};
});
