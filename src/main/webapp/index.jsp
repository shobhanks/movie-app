<!DOCTYPE html>
<html lang="en" ng-app="CineTicketApp">
<head>
<meta charset="utf-8">
<title>CineTicket</title>
<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="css/beautify.css" type="text/css" />
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js"></script>
<script src="js/SearchController.js"></script>
<style>
h1 {
	font-size: 40px;
	font-family: Monotype Corsiva;
	text-align: center;
}
</style>
</head>
<div class="container">
	<h1>~~CineTicket~~</h1>
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container">
				<ul class="nav">
					<li><a href="index.jsp">Home</a></li>
					<li><a href="signin.html">Sign In</a></li>
					<li><a href="signup.html">Join</a></li>
					<li><a href="theater.html">Theater</a></li>
					<li><a href="trailer.html">Trailers</a></li>
				</ul>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="span12">
			<div ng-controller="searchController">
				<h3>Find Movies</h3>
				Title <input ng-model="searchByTitle" /> 
				Actor <input ng-model="searchByActor" />
				Genre <input ng-model="searchByGenre" />
				<br>
				<br>
				<a class="btn btn-success"
					ng-click="search()"><i class="icon-search"></i>Search</a>
					<br>
					<br>
				<div>
					<table class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th>Title</th>
								<th>Description</th>
								<th>Date Of Release</th>
								<th>USERRATING</th>
								<th>IMDBRATING</th>
								<th>USERREVIEWS</th>
								<th>IMAGE</th>
								<th>GENRE</th>
								<th>MPARATING</th>
							</tr>
						</thead>
						<tbody>
							<tr class="animate-repeat" ng-repeat="movie in movies">
								<td>{{movie.title}}</td>
								<td>{{movie.description}}</td>
								<td>{{movie.dateOfRelease}}</td>
								<td>{{movie.userRating}}</td>
								<td>{{movie.imdbRating}}</td>
								<td>{{movie.userReviews}}</td>
								<td><img src="{{movie.image}}" ></td>
								<td>{{movie.genre}}</td>
								<td>{{movie.mparating}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<body>



</body>
</html>
