package org.shobhank.ticket.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name = "ticket_details")
public class Ticket {
	@Id
	@Column(name="ti_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int ti_id;
	@ManyToOne(targetEntity=ShowTime.class)
	@JoinColumn(name="s_id")	
	private ShowTime showtime;
	public ShowTime getShowtime() {
		return showtime;
	}
	public void setShowtime(ShowTime showtime) {
		this.showtime = showtime;
	}
	public int getTi_id() {
		return ti_id;
	}
	public void setTi_id(int ti_id) {
		this.ti_id = ti_id;
	}

}
