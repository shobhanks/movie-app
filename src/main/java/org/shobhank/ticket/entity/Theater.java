package org.shobhank.ticket.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "theater_details")
public class Theater {
	@Id
	@Column(name="t_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int t_id;
	private String name;
	private int zipCode;
	private String address;
	private int auditorium;
    public int getAuditorium() {
		return auditorium;
	}
	public void setAuditorium(int auditorium) {
		this.auditorium = auditorium;
	}
	@ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(name="movie_theater", 
                joinColumns={@JoinColumn(name="T_ID")}, 
                inverseJoinColumns={@JoinColumn(name="M_ID")})
    private Set<Movie> movies = new HashSet<Movie>();
	public Set<Movie> getMovies() {
		return movies;
	}
	public void setMovies(Set<Movie> movies) {
		this.movies = movies;
	}
	public int getT_id() {
		return t_id;
	}
	public void setT_id(int t_id) {
		this.t_id = t_id;
	}
	public void setAddress(String address) {
		this.address = address;
	}



	public int getZipCode() {
		return zipCode;
	}
	// Setters are just for testing to be removed later
	public void setZipCode(int zipCode) {
		this.zipCode = zipCode;
	}
	public String getAddress() {
		return address;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
