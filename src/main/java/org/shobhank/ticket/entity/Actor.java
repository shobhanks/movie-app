package org.shobhank.ticket.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "actor_details")
public class Actor {
	@Id
	@Column(name="a_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int a_id;
	@Column(name = "name", unique = true, nullable = false)
	private String name;
	private Date dateofbirth;
	private String country;
	private String state;
	private String awards;
	private byte[] image;
    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(name="movie_actor", 
                joinColumns={@JoinColumn(name="A_ID")}, 
                inverseJoinColumns={@JoinColumn(name="M_ID")})
	private Set<Movie> movies = new HashSet<Movie>();
	public Set<Movie> getMovies() {
		return movies;
	}
	public void setMovies(Set<Movie> movies) {
		this.movies = movies;
	}
	public int getA_id() {
		return a_id;
	}
	public void setA_id(int a_id) {
		this.a_id = a_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getDateofbirth() {
		return dateofbirth;
	}
	public void setDateofbirth(Date dateofbirth) {
		this.dateofbirth = dateofbirth;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getAwards() {
		return awards;
	}
	public void setAwards(String awards) {
		this.awards = awards;
	}
	public byte[] getImage() {
		return image;
	}
	public void setImage(byte[] image) {
		this.image = image;
	}
	
}
