package org.shobhank.ticket.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name = "showtime_details")
public class ShowTime {
	@Id
	@Column(name="s_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int s_id;
	private Date showtime;
	@ManyToOne(targetEntity=Theater.class)
	@JoinColumn(name="t_id")	
	private Theater theater;
	public Theater getTheater() {
		return theater;
	}
	public void setTheater(Theater theater) {
		this.theater = theater;
	}
	public int getS_id() {
		return s_id;
	}
	public void setS_id(int s_id) {
		this.s_id = s_id;
	}
	public Date getShowtime() {
		return showtime;
	}
	public void setShowtime(Date showtime) {
		this.showtime = showtime;
	}
}
