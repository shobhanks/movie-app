package org.shobhank.ticket.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



@Entity
@Table (name = "trailer_details")
public class Trailer {
	@Id
	@Column(name="tr_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int tr_id;
	private String link;

	@ManyToOne(targetEntity=Movie.class)
	@JoinColumn(name="m_id")	
	private Movie movie;
	
	public int getTr_id() {
		return tr_id;
	}

	public void setTr_id(int tr_id) {
		this.tr_id = tr_id;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}


	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	

}
