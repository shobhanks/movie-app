package org.shobhank.ticket.entity;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;



@Entity
@Table(name = "movie_details")
public class Movie {
	@Id
	@Column(name="m_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int m_id;
	@Column(name = "title", unique = true, nullable = false)
	private String title;
	private String description;
	private Date dateOfRelease;
	private int userRating;
	private int imdbRating;
	private String userReviews;
	private String image;
	private String genre;
	private String mparating;
    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(name="movie_actor", 
                joinColumns={@JoinColumn(name="M_ID")}, 
                inverseJoinColumns={@JoinColumn(name="A_ID")})
	private Set<Actor> actors = new HashSet<Actor>();
    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(name="movie_theater", 
                joinColumns={@JoinColumn(name="M_ID")}, 
                inverseJoinColumns={@JoinColumn(name="T_ID")})
    private Set<Theater> theaters = new HashSet<Theater>();
	public Set<Theater> getTheaters() {
		return theaters;
	}
	public void setTheaters(Set<Theater> theaters) {
		this.theaters = theaters;
	}
	public Set<Actor> getActors() {
		return actors;
	}
	public void setActors(Set<Actor> actors) {
		this.actors = actors;
	}
	public Date getDateOfRelease() {
		return dateOfRelease;
	}
	public int getUserRating() {
		return userRating;
	}
	public int getImdbRating() {
		return imdbRating;
	}
	public String getUserReviews() {
		return userReviews;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	// Setters are just for testing to be removed later
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTitle() {
		return title;
	}
	public String getDescription() {
		return description;
	}

	public String getMparating() {
		return mparating;
	}
	public void setMparating(String mparating) {
		this.mparating = mparating;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}



}
