package org.shobhank.ticket.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.shobhank.ticket.entity.Actor;
import org.shobhank.ticket.service.impl.BrowseActor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Path("/actorsearch")
public class ActorBrowse {
	
	@Autowired
	@Qualifier(value="browseActor")
	BrowseActor browseActor;
	
	@GET
	public Response search(@QueryParam("name") String name) throws JSONException {
		String output = "";
		List<Actor> actors = browseActor.getActorInfo(name);
		if(actors==null)
			return Response.status(204).entity("No Content for " + name).build();
		JSONArray jsonarray = new JSONArray();
		for(int i=0;i< actors.size();i++){
			Actor actor = actors.get(i);
			JSONObject jsonobject = new JSONObject();
			jsonobject.put("name", actor.getName());
			jsonobject.put("dateofbirth", actor.getDateofbirth());
			jsonobject.put("country", actor.getCountry());
			jsonobject.put("state", actor.getState());
			jsonobject.put("awards", actor.getAwards());
			jsonarray.put(jsonobject);
		}
		
		output = jsonarray.toString();
		if (!output.equals(""))
			return Response.status(200).entity(output).build();
		else
			return Response.status(204).entity("No Content for " + name).build();
 
	}

}
