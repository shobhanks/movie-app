package org.shobhank.ticket.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.shobhank.ticket.entity.Movie;
import org.shobhank.ticket.entity.Trailer;
import org.shobhank.ticket.service.BrowseTrailer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Path("/trailer")
public class TrailerBrowse {
	@Autowired
	@Qualifier(value="browseTrailerImpl")
	BrowseTrailer browseTrailer;
	
	@GET
	public Response search(@QueryParam("title") String title) throws JSONException {
		
		String output = "";
		List<Trailer> trailers = browseTrailer.getMovieInfo(title);
		if(trailers==null)
			return Response.status(204).entity("No Content").build();
		JSONArray jsonarray = new JSONArray();
		for(int i=0;i< trailers.size();i++){
			Trailer trailer = trailers.get(i);
			JSONObject jsonobject = new JSONObject();
			jsonobject.put("movie", trailer.getMovie().getTitle());
			jsonobject.put("link", trailer.getLink());
			jsonarray.put(jsonobject);
		}
		
		output = jsonarray.toString();
		if (!output.equals(""))
			return Response.status(200).entity(output).build();
		else
			return Response.status(204).entity("No Content").build();
	}
	
}
