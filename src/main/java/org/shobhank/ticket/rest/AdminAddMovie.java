package org.shobhank.ticket.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;


@Path("/admin")
public class AdminAddMovie {
	
	@POST
	@Path("/addmovie")
	public Response addMovie(String imdbids){
		String ids ="";
		JsonFactory jfactory = new JsonFactory();
		JsonParser jParser = null;
		try {
			jParser = jfactory.createJsonParser(imdbids);
			while (jParser.nextToken() != JsonToken.END_OBJECT) {
				String fieldname = jParser.getCurrentName();
				if ("imdbids".equals(fieldname)) {
				  jParser.nextToken();
				  ids = jParser.getText(); 
				}
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		Connection conn = null;
		int flag = 0;
		String output = ""
;		try {
			String s = null;
			String command = "python /Users/shsharma/Documents/TestWorkSpace/MySQL-python-1.2.4b4/imdbget.py";
			String[] idlist = ids.split(",");
			for(int i=0;i<idlist.length;i++){
			command = command + " " + idlist[i];
			}
			Process p = Runtime.getRuntime().exec(command);
			BufferedReader stdInput = new BufferedReader(new 
	                 InputStreamReader(p.getInputStream()));
		    while ((s = stdInput.readLine()) != null) {
		    	System.out.println(s);
		    	String[] movie = s.split("::");
    			Class.forName("com.mysql.jdbc.Driver");
    			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb","root", "abc123");
    			String insertTableSQL = "INSERT INTO movie_details"
    					+ "(title, description, dateofrelease, userrating, imdbrating, userreviews,"
    					+ "image,genre,mparating) VALUES"
    					+ "(?,?,?,?,?,?,?,?,?)";
    			PreparedStatement ps = conn.prepareStatement(insertTableSQL);
    			ps.setString(1, movie[0]);
    			output = output + movie[0];
    			ps.setString(2, movie[1]);
    			ps.setDate(3, new Date(178787878));
    			ps.setInt(4, (int) Double.parseDouble(movie[2]));
    			ps.setInt(5, (int) Double.parseDouble(movie[3]));
    			ps.setString(6, "Good");
    			ps.setString(7, movie[4]);
    			ps.setString(8, movie[5]);
    			ps.setString(9, "PG13");
    			flag = ps.executeUpdate();
    			conn.close();			
    		} }catch (ClassNotFoundException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (SQLException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		if(flag!=0)
			return Response.status(200).entity("Created Successfully Movie(s) " + output).build();
		else
			return Response.status(404).entity("Failed").build();
	}
}
