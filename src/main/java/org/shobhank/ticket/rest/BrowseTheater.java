package org.shobhank.ticket.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.shobhank.ticket.service.BrowseTheaterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.shobhank.ticket.entity.Theater;

@Component
@Path("/searchTheater")
public class BrowseTheater {
	
	@Autowired
	@Qualifier(value="browseTheaterServiceByZipCode")
	BrowseTheaterService browseTheaterService;
	
	@GET
	public Response search(@QueryParam("zipCode") int zip) throws JSONException {
		String output = "";
		List<Theater> result = browseTheaterService.getTheaterInfo(zip);
		if(result==null)
			return Response.status(204).entity("No Content").build();
		JSONArray jsonarray = new JSONArray();
		for(int i=0;i< result.size();i++){
			Theater theater = result.get(i);
			JSONObject jsonobject = new JSONObject();
			jsonobject.put("Name",theater.getName());
			jsonobject.put("Address",theater.getAddress());
			String mapurl = "https://www.google.com/maps/embed/v1/place?key=AIzaSyDFu2W97cuzUs2CDo1s6sGZUmlkoyvQXeI%20&q="+
			theater.getAddress();
			jsonobject.put("mapurl", mapurl);
			jsonarray.put(jsonobject);
		}
		
		output = jsonarray.toString();
		if (!output.equals(""))
			return Response.status(200).entity(output).build();
		else
			return Response.status(204).entity("No Content").build();
 
	}
}
