package org.shobhank.ticket.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.shobhank.ticket.entity.Movie;
import org.shobhank.ticket.service.BrowseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Path("/searchByActor")
public class MovieBrowseByActor {
	
	@Autowired
	@Qualifier(value="BrowseServiceByActor")
	BrowseService browseService;
	
	@GET
	public Response search(@QueryParam("actor") String actor) throws JSONException {
		String output = "";
		List<Movie> movies = browseService.getMovieInfo(actor);
		if(movies==null)
			return Response.status(204).entity("No Content").build();
		JSONArray jsonarray = new JSONArray();
		for(int i=0;i< movies.size();i++){
			Movie movie = movies.get(i);
			JSONObject jsonobject = new JSONObject();
			jsonobject.put("title", movie.getTitle());
			jsonobject.put("description",movie.getDescription());
			jsonobject.put("dateOfRelease",movie.getDateOfRelease());
			jsonobject.put("userRating",movie.getUserRating());
			jsonobject.put("imdbRating",movie.getImdbRating());
			jsonobject.put("userReviews",movie.getUserReviews());
			jsonobject.put("genre",movie.getGenre());
			jsonobject.put("mparating",movie.getMparating());
			jsonarray.put(jsonobject);
		}
		
		output = jsonarray.toString();
		if (!output.equals(""))
			return Response.status(200).entity(output).build();
		else
			return Response.status(204).entity("No Content").build();
 
	}
}
