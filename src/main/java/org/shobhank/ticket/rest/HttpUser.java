package org.shobhank.ticket.rest;



public class HttpUser {
	private String uname;
	private String email_id;
	private String fav_genre;
	private String user_pwd;
	private int zipcode;
	public String getEmail_id() {
		return email_id;
	}
	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}
	public String getFav_genre() {
		return fav_genre;
	}
	public void setFav_genre(String fav_genre) {
		this.fav_genre = fav_genre;
	}
	public String getUser_pwd() {
		return user_pwd;
	}
	public void setUser_pwd(String user_pwd) {
		this.user_pwd = user_pwd;
	}
	public int getZipcode() {
		return zipcode;
	}
	public void setZipcode(int zipcode) {
		this.zipcode = zipcode;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
}
