package org.shobhank.ticket.rest;

import java.io.IOException;
import java.net.URI;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;
import org.shobhank.ticket.service.AccountService;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.QueryParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;



@Component
@Path("/account")
public class AccountServiceRest {
	
	@Autowired
	@Qualifier(value="accountServiceImpl")
	AccountService accountService;
	
	@GET
	@Path("/login")
	public Response login(@QueryParam("username") String uname,@QueryParam("password") String pwd) {
		String output = "";
		boolean result = accountService.verifyUser(uname, pwd);
		if(result == true)
			{
			output = "Successful";
			URI myUri = URI.create("http://localhost:8080/CineTicket3/index.jsp");
			return Response.status(200).entity(output).cookie(new NewCookie("CTCookie",uname)).build();	
			}
		else{
			output = "Failed";
			return Response.status(200).entity(output).build();
		}
	}
	
	//createAccount(String uname, String user_pwd, String email_id, int zipcode, String fav_genre)
	@POST
	@Path("/signup")
	public Response signup(String user) throws JsonParseException, IOException{
		JsonFactory jfactory = new JsonFactory();
		JsonParser jParser = jfactory.createJsonParser(user);
		String uname=null,pwd=null,email_id=null,fav_genre=null;
		int zipcode=0;
		while (jParser.nextToken() != JsonToken.END_OBJECT) {
			String fieldname = jParser.getCurrentName();
			if ("uname".equals(fieldname)) {
			  jParser.nextToken();
			  uname = jParser.getText(); 
			}
			if ("pwd".equals(fieldname)) {
				  jParser.nextToken();
				  pwd = jParser.getText(); 
				}
			if ("email_id".equals(fieldname)) {
				  jParser.nextToken();
				  email_id = jParser.getText(); 
				}
			if ("zipcode".equals(fieldname)) {
				  jParser.nextToken();
				  zipcode = Integer.parseInt(jParser.getText()); 
				}
			if ("fav_genre".equals(fieldname)) {
				  jParser.nextToken();
				  fav_genre = jParser.getText(); 
				}
		}
		boolean status = accountService.createAccount(uname, pwd, email_id, zipcode, fav_genre);
		if(status==true)
			return Response.status(200).entity("Success in creating " 
		+ uname + "\t" + pwd + "\t" + email_id + "\t" + zipcode + "\t" + fav_genre).build();
		else 
			return Response.status(409).entity("Failed to create user please contact helpdesk").build();
	}
	
}
