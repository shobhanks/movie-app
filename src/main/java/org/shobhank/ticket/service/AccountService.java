package org.shobhank.ticket.service;

public interface AccountService {
	public boolean createAccount(String uname, String user_pwd, String email_id, int zipcode, String fav_genre);
	public boolean verifyUser(String uname,String pwd);
}
