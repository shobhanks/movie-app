package org.shobhank.ticket.service;

import java.util.List;

import org.shobhank.ticket.entity.Trailer;

public interface BrowseTrailer {
	public List<Trailer> getMovieInfo(String title);
}
