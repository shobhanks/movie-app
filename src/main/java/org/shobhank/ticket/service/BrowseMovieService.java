package org.shobhank.ticket.service;

import java.util.List;

import org.shobhank.ticket.entity.Movie;

public interface BrowseMovieService {
	public List<Movie> getMovieInfo(String title,String actor,String genre);
}
