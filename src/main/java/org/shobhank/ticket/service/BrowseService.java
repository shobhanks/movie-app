package org.shobhank.ticket.service;

import java.util.List;

import org.shobhank.ticket.entity.Movie;

public interface BrowseService {
	public List<Movie> getMovieInfo(String value);
}
