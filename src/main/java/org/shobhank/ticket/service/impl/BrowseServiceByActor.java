package org.shobhank.ticket.service.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.shobhank.ticket.entity.Movie;
import org.shobhank.ticket.service.BrowseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component ("BrowseServiceByActor")
public class BrowseServiceByActor implements BrowseService {
	@Autowired
	private SessionFactory sessionFactory;
	
	/*
	 * This method returns movies 
	 * based on actor name
	 * @param value
	 * @return movieList
	 */
	public List<Movie> getMovieInfo(String value) {
		List<Movie> movieList = getMovieList(value);
		return movieList;
	}
	
	/*
	 * This method would be implemented to fetch movies from database
	 * having actor name passed
	 * @param name
	 * @return listOfMovies
	 */
	private List<Movie> getMovieList(String name){
		/*
		 * Add query to fetch movie list from database for this actor
		 */
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("Select distinct m from Movie m join m.actors a where a.name=:name");
		query.setParameter("name", name);
		@SuppressWarnings("unchecked")
		List<Movie> listOfMovies = query.list();
		if(listOfMovies.size()==0)
			return null;
		return listOfMovies;
	}


}
