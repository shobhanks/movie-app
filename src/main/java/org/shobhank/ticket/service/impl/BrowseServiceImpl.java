package org.shobhank.ticket.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.shobhank.ticket.entity.Movie;
import org.shobhank.ticket.service.BrowseMovieService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class BrowseServiceImpl implements BrowseMovieService {
	Logger logger = Logger.getLogger(this.getClass());
	@Autowired
	private SessionFactory sessionFactory;
	
	/*
	 * This method returns movies 
	 * based on movie title
	 * @param value
	 * @return movieList
	 */
	public List<Movie> getMovieInfo(String title,String actor,String genre) {
		List<Movie> movieList = getMovieList(title,actor,genre);
		return movieList;
	}
	
	/*
	 * This method would be implemented to fetch movies from database
	 * containing the argument passed
	 * @param name
	 * @return listOfMovies
	 */
	private List<Movie> getMovieList(String title,String actor,String genre){
		/* To be Done
		 * Add query to fetch movie list from database 
		 */
		Query query1=null,query2=null,query3=null;
		List<Movie> listOfMovies1 = null,listOfMovies2 = null,listOfMovies3 = null;
		Session session = sessionFactory.openSession();
        
		if(title!=null && !title.isEmpty()){
			query1 = session.createQuery("Select distinct m from Movie m where m.title like ?"); // m.genre like ?
			query1.setString(0, "%"+title+"%");
			listOfMovies1 = query1.list();
		}
		
		if(genre!=null && !genre.isEmpty()){
			query2 = session.createQuery("Select distinct m from Movie m where m.genre like ?"); // m.genre like ?
			query2.setString(0, "%"+genre+"%");
			listOfMovies2 = query2.list();
		}

		if(actor!=null && !actor.isEmpty())
			{
			query3 = session.createQuery("Select distinct m from Movie m join m.actors a where a.name like :actor");
			query3.setParameter("actor", actor);
			listOfMovies3 = query3.list();
			}

		
 		if(listOfMovies1!=null && listOfMovies2!=null && listOfMovies3!=null)
			return null;
 		List<Movie> listOfMovies = new ArrayList<Movie>();
 		
 		HashSet<Movie> tempHashSet = new HashSet<Movie>();
 		
 		HashSet<Movie> tempHashSet1 = new HashSet<Movie>();
 		if (listOfMovies1!=null)
 			tempHashSet1.addAll(listOfMovies1);
 		
 		HashSet<Movie> tempHashSet2 = new HashSet<Movie>();
 		if (listOfMovies2!=null)
 			tempHashSet2.addAll(listOfMovies2);
 		
 		HashSet<Movie> tempHashSet3 = new HashSet<Movie>();
 		if (listOfMovies3!=null)
 			tempHashSet3.addAll(listOfMovies3);
 		
 		tempHashSet.addAll(tempHashSet1);
 		tempHashSet.addAll(tempHashSet2);
 		tempHashSet.addAll(tempHashSet3);
 		
 		listOfMovies.clear();
 		listOfMovies.addAll(tempHashSet);
		return listOfMovies;
	}

}
