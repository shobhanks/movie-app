package org.shobhank.ticket.service.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.shobhank.ticket.entity.Actor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BrowseActor {
	@Autowired
	private SessionFactory sessionFactory;

	/*
	 * This method returns actors 
	 * based on actor name
	 * @param value
	 * @return actorList
	 */
	public List<Actor> getActorInfo(String value) {
		List<Actor> actorList = getActorList(value);
		return actorList;
	}
	
	private List<Actor> getActorList(String name){

		Session session = sessionFactory.openSession();
		Query query = session.createQuery("from Actor where name like ?");
		query.setString(0, "%"+name+"%");
		@SuppressWarnings("unchecked")
		List<Actor> listOfActors = query.list();
		if(listOfActors.size()==0)
			return null;
		return listOfActors;
	}

}
