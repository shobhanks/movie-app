package org.shobhank.ticket.service.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.shobhank.ticket.entity.User;
import org.shobhank.ticket.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;



@Component
public class AccountServiceImpl implements AccountService {
	
	@Autowired
	private SessionFactory sessionFactory;
	

	public boolean createAccount(String uname, String user_pwd, String email_id, int zipcode, String fav_genre) {
		Session session = sessionFactory.openSession();
		Transaction tr = session.beginTransaction();
		User user = new User();
		user.setUname(uname);
		user.setUser_pwd(user_pwd);
		user.setEmail_id(email_id);
		user.setZipcode(zipcode);
		user.setFav_genre(fav_genre);
		try{
			session.save(user);
			tr.commit();
		}catch(Exception e){
			return false;
		}
		return true;
	}

	public boolean verifyUser(String uname, String pwd) {
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("from User where uname=:uname and user_pwd=:pwd");
		query.setParameter("uname", uname);
		query.setParameter("pwd", pwd);
		@SuppressWarnings("unchecked")
		List<User> user = query.list();
		if(user.size()==0)
			return false;
		return true;
	}

}
