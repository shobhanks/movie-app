package org.shobhank.ticket.service.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.shobhank.ticket.entity.Movie;
import org.shobhank.ticket.service.BrowseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BrowseServiceByTitle implements BrowseService{
	@Autowired
	private SessionFactory sessionFactory;

	/*
	 * This method returns movies 
	 * based on movie title
	 * @param value
	 * @return movieList
	 */
	public List<Movie> getMovieInfo(String value) {
		List<Movie> movieList = getMovieList(value);
		return movieList;
	}
	
	/*
	 * This method would be implemented to fetch movies from database
	 * containing the argument passed
	 * @param name
	 * @return listOfMovies
	 */
	private List<Movie> getMovieList(String name){
		/* To be Done
		 * Add query to fetch movie list from database 
		 */
		
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("from Movie where title like ?");
		query.setString(0, "%"+name+"%");
		@SuppressWarnings("unchecked")
		List<Movie> listOfMovies = query.list();
		if(listOfMovies.size()==0)
			return null;
		return listOfMovies;
	}

}
