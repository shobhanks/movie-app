package org.shobhank.ticket.service.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.shobhank.ticket.entity.Theater;
import org.shobhank.ticket.service.BrowseTheaterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BrowseTheaterServiceByZipCode implements BrowseTheaterService {
	@Autowired
	private SessionFactory sessionFactory;

	/*
	 * This method returns Theaters
	 * based on zip code
	 * @param value
	 * @return theaterInfo
	 */
	public List<Theater> getTheaterInfo(int zipCode) {
		List<Theater> getTheaterInfo = getTheaterList(zipCode);
		return getTheaterInfo;
	}
	private List<Theater> getTheaterList(int zipCode){
		/* To be Done
		 * Add query to fetch movie list from database 
		 */
		
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("from Theater where zipCode=?");
		query.setInteger(0, zipCode);
		@SuppressWarnings("unchecked")
		List<Theater> listOfTheaters = query.list();
		if(listOfTheaters.size()==0)
			return null;
		return listOfTheaters;
	}

}
