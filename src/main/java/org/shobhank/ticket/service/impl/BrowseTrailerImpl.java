package org.shobhank.ticket.service.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.shobhank.ticket.entity.Trailer;
import org.shobhank.ticket.service.BrowseTrailer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;



@Component
public class BrowseTrailerImpl implements BrowseTrailer {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Trailer> getMovieInfo(String title) {
		Query query1=null;
		List<Trailer> listOfTrailers = null;
		Session session = sessionFactory.openSession();
        
		if(title!=null && !title.isEmpty()){
			query1 = session.createQuery("Select distinct t from Trailer t join t.movie m where m.title like :title");
			query1.setParameter("title", title);
			listOfTrailers = query1.list();
		}
		return listOfTrailers;	
	}

}
