package org.shobhank.ticket.service;

import java.util.List;

import org.shobhank.ticket.entity.Theater;

public interface BrowseTheaterService {
	public List<Theater> getTheaterInfo(int zipCode);
}
