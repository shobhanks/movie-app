package org.shobhank.ticket.service;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class TestBrowseTheaterServiceByZipCode extends AbstractJUnit4SpringContextTests{
	
	@Autowired
	@Qualifier(value="browseTheaterServiceByZipCode")
	private BrowseTheaterService browseTheaterService;
	
	@Test
	public void testBrowseService(){
		System.out.println(browseTheaterService.getTheaterInfo(98321).size());
		Assert.assertEquals("IMAX 20", browseTheaterService.getTheaterInfo(98321).get(0).getName());		}
	@Test
	public void testBrowseServiceNotFound(){
		Assert.assertEquals(null, browseTheaterService.getTheaterInfo(9));		}
}
