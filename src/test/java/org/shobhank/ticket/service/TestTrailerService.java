package org.shobhank.ticket.service;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class TestTrailerService extends AbstractJUnit4SpringContextTests{

	@Autowired
	@Qualifier(value="browseTrailerImpl")
	private BrowseTrailer browseTrailer;
	
	@Test
	public void testTrailerService(){
		System.out.println(browseTrailer.getMovieInfo("The Pink Panther").get(0).getLink());
		Assert.assertEquals("true","true");
	}
}
