package org.shobhank.ticket.service;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class TestAccountService extends AbstractJUnit4SpringContextTests{
	
	@Autowired
	@Qualifier(value="accountServiceImpl")
	private AccountService accountService;
	
	@Test
	public void testGoodAccountLogin(){
		Assert.assertEquals(true,accountService.verifyUser("shobhanks", "abc123"));
	}
	
	@Test
	public void testBadAccountLogin(){
		Assert.assertEquals(false,accountService.verifyUser("shobhanks", "abc"));
	}
	
//	@Test
//	public void testGoodAccountCreate(){
//		Assert.assertEquals(true,accountService.createAccount("testuser1", "abc123", "abc@xyz.com",95126,"Comedy"));
//	}
	
	@Test
	public void testBadAccountCreate(){
		Assert.assertEquals(false,accountService.createAccount(null, "abc123", "abc@xyz.com",95126,"Comedy"));
	}
}
