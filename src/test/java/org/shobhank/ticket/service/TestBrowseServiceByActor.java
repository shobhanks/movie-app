package org.shobhank.ticket.service;


import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class TestBrowseServiceByActor extends AbstractJUnit4SpringContextTests {
	
	@Autowired
	@Qualifier(value="BrowseServiceByActor")
	private BrowseService browseService;
	
	@Test
	public void testBrowseService(){
		Assert.assertEquals("Iron Man", browseService.getMovieInfo("Robert Downey Jr").get(0).getTitle());
		}
	@Test
	public void testBrowseServiceNotFound(){
		Assert.assertEquals(null, browseService.getMovieInfo("To Cruise"));
		}
}
